<?php
declare(strict_types=1);

$base = '/router/public';

require_once '../src/Router.php';

$router = new Router();

$router->get($base . '/', function() {
    $title = 'Home';
    require_once __DIR__ . '/../templates/home.phtml';
});

$router->get($base . '/about',  function() {
    $title = 'About';
    require_once __DIR__ . '/../templates/about.phtml';
});

$router->get($base . '/contact', function() {
    $title = 'Contact';
    require_once __DIR__ . '/../templates/contact.phtml';
});

if(in_array('Admin', $user_rights)) {
    $router->get($base . '/admin', function(array $params = []) {
        $title = 'Admin';
        require_once __DIR__ . '/../templates/admin.phtml';
    });

    $router->post($base . '/admin/log', function($params) {
        $title = 'Admin logs';
        require_once __DIR__ . '/../templates/admin_log.phtml';
    });
}

if(in_array('Upload', $user_rights)) {
    $router->get($base . '/upload', function(array $params = []) {
        $title = 'Upload';
        require_once __DIR__ . '/../templates/upload.phtml';
    });
}

/*
 * 
 * Special Routes
 * 
 */ 

$router->addNotFoundHandler(function() {
    $title = 'Page not found';
    require_once __DIR__ . '/../templates/404.phtml';
});


$router->run();